package Deserializer;

import Structure.*;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BlockDeserializer implements JsonDeserializer<Block> {

    public Block deserialize(JsonElement json, Type typeOf, JsonDeserializationContext context) throws JsonParseException {

        if(!json.isJsonObject()){
            throw new JsonParseException("ошибка при десериализации json");
        }

        Block block;

        JsonObject jsonObject = json.getAsJsonObject();

        if(jsonObject.has("optype")){
            switch (jsonObject.get("optype").getAsString()){
                case ("Select"):
                    block = SelectDeserialize(json);
                    break;
                case ("dep"):
                    block = ImportDeserialize(json);
                    break;
                case ("virtual"):
                    block = VirtualDeserialize(json);
                    break;
                default:
                    block = new Block(jsonObject);
            }
        }
        else if(!jsonObject.has("next") && !jsonObject.has("children")){
            block = VirtualDeserialize(json);
        }
        else {
            block = ActionDeserialize(json);
        }
        return block;
    }

    private Action ActionDeserialize(JsonElement json){

        JsonObject jsonObject = json.getAsJsonObject();

        String name = jsonObject.get("name").getAsString();
        jsonObject.remove("name");
        String next = jsonObject.get("next").getAsString();
        jsonObject.remove("next");
        String msg;
        if(jsonObject.has("msg")) {
            msg = jsonObject.get("msg").getAsString();
            jsonObject.remove(msg);
        }
        else msg = null;
        if(jsonObject.size() == 0) jsonObject = null;

        Action action = new Action(name, msg, next, jsonObject);

        return action;
    }

    private Virtual VirtualDeserialize(JsonElement json){

        JsonObject jsonObject = json.getAsJsonObject();

        String name = jsonObject.get("name").getAsString();
        jsonObject.remove("name");
        String optype;
        if(jsonObject.has("optype")) {
            optype = jsonObject.get("optype").getAsString();
            jsonObject.remove("optype");
        }
        else optype = null;

        if(jsonObject.size() == 0) jsonObject = null;

        Virtual virtual = new Virtual(name, optype, jsonObject);

        return virtual;
    }

    private Import ImportDeserialize(JsonElement json){

        JsonObject jsonObject = json.getAsJsonObject();

        String name = jsonObject.get("name").getAsString();
        jsonObject.remove("name");
        String msg;
        if(jsonObject.has("msg")){
             msg = jsonObject.get("msg").getAsString();
             jsonObject.remove("msg");
        }
        else msg = null;

        String dep = jsonObject.get("dep").getAsString();
        jsonObject.remove("dep");

        Map<String, String> children = getList("children", jsonObject);

        jsonObject.remove("children");
        jsonObject.remove("optype");

        if(jsonObject.size() == 0) jsonObject = null;
        Import _import = new Import(name, msg, dep, children, jsonObject);

        return _import;
    }

    private Select SelectDeserialize(JsonElement json){

        JsonObject jsonObject = json.getAsJsonObject();

        String name = jsonObject.get("name").getAsString();
        jsonObject.remove("name");
        String msg;
        if(jsonObject.has("msg")) {
            msg = jsonObject.get("msg").getAsString();
            jsonObject.remove("msg");
        }
        else msg = null;

        Map<String, String> labels = getList("labels", jsonObject);
        jsonObject.remove("labels");

        Map<String, String> children = getList("children", jsonObject);
        jsonObject.remove("children");

        if(jsonObject.size() == 0) jsonObject = null;
        Select select = new Select(name, msg, labels, children, jsonObject);
        return select;
    }

    private Map<String, String> getList(String blockKey, JsonObject jsonObject) throws JsonParseException{

        Map<String, String> hashMap = new LinkedHashMap<>();
        if(jsonObject.get(blockKey).isJsonArray()){
            JsonArray jsonArray = jsonObject.get(blockKey).getAsJsonArray();
            for(int i = 0; i < jsonArray.size(); i++){
                if(!jsonArray.get(i).isJsonPrimitive()){
                    throw new JsonParseException("ошибка при десериализации json");
                }
                JsonPrimitive elem = jsonArray.get(i).getAsJsonPrimitive();
                String value = elem.getAsString();
                hashMap.put(String.valueOf(i), value);
            }
        }
        else if(jsonObject.get(blockKey).isJsonObject()){
            JsonObject jsonObjectC = jsonObject.getAsJsonObject(blockKey);
            for (Map.Entry<String, JsonElement> entry : jsonObjectC.entrySet()){
                String key = entry.getKey();
                if(!jsonObjectC.get(key).isJsonPrimitive()){
                    throw new JsonParseException("ошибка при десериализации json");
                }
                String value = jsonObjectC.get(key).getAsString();
                hashMap.put(key, value);

            }
        }
        else{
            throw new JsonParseException("ошибка при десеализации json");
        }

        return hashMap;
    }
}
