package Deserializer;

import Structure.Block;
import Structure.Diagram;
import Structure.StringListNode;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.*;

public class DiagramDeserializer implements JsonDeserializer<Diagram> {

    @Override
    public Diagram deserialize(JsonElement json, Type TypeofT, JsonDeserializationContext context) throws JsonParseException {

        Diagram diagram = new Diagram();
        if(!json.isJsonObject()) {
            throw new JsonParseException("Ошибка при десериализации json");
        }

        JsonObject jsonObject = json.getAsJsonObject();
        diagram.root = jsonObject.get("root").getAsString();
        diagram.outs = new LinkedHashMap<>();

        if(jsonObject.get("outs").isJsonArray()){
            int i = 0;
            for(JsonElement elem : jsonObject.get("outs").getAsJsonArray()){
                if(!elem.isJsonPrimitive()){
                    throw new JsonParseException("ошибка при десериализации json");
                }
                diagram.outs.put(String.valueOf(i), elem.getAsString());
                i++;
            }
        }
        else if(jsonObject.get("outs").isJsonObject()){
            for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()){
                String key = entry.getKey();
                String value = entry.getValue().getAsString();
                diagram.outs.put(key, value);
            }
        }

        if(jsonObject.has("name")){
            diagram.name = jsonObject.get("name").getAsString();
        }
        else {
            diagram.name = null;
        }

        if(jsonObject.has("src")){
            diagram.src = jsonObject.get("src").getAsString();
        }
        else {
            diagram.src = null;
        }

        JsonElement blockView = jsonObject.get("blocks");
        if(!blockView.isJsonObject()){
            throw new JsonParseException("Ошибка при десериализации json");
        }
        JsonObject blocks = blockView.getAsJsonObject();
        Map<String, Block> blocksList = new HashMap<>();
        for (Map.Entry<String, JsonElement> entry : blocks.entrySet()){
            String block_id = entry.getKey();
            Block block = context.deserialize(entry.getValue(), Block.class);
            blocksList.put(block_id, block);
        }
        diagram.blocks = new HashMap<>(blocksList);

        return diagram;
    }
}
