package Deserializer;

import Structure.Diagram;
import Structure.Source;
import com.google.gson.*;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SourceDeserializer implements JsonDeserializer<Source> {

    @Override
    public Source deserialize(JsonElement json, Type TypeofT, JsonDeserializationContext context) throws JsonParseException {

        Source source;

        if(!json.isJsonObject()) {
            throw new JsonParseException("Ошибка при десериализации json");
        }
        JsonObject jsonObject = json.getAsJsonObject();
        String main = jsonObject.get("rootDiagram").getAsString();

        JsonElement diagramView = jsonObject.get("diagrams");
        if(!diagramView.isJsonObject()){
            throw new JsonParseException("Ошибка при десериализации json");
        }
        JsonObject diagrams = diagramView.getAsJsonObject();
        //List<DiagramNode> diagramsList = new LinkedList<>();
        Map<String, Diagram> diagramsList = new HashMap<>();
        for(Map.Entry<String, JsonElement> entry : diagrams.entrySet()){
            String diagram_id = entry.getKey();
            Diagram diagram = context.deserialize(entry.getValue(), Diagram.class);
            //DiagramNode diagramNode = new DiagramNode(diagram_id, diagram);
            diagramsList.put(diagram_id, diagram);
        }

        source = new Source(main, diagramsList);

        return source;
    }
}
