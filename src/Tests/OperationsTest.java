package Tests;

import Operations.Operations;
import Service.ReadFile;
import Service.OperationException;
import Service.Split;
import Service.WriteFile;
import Structure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {

    public Source source;
    public String input;
    public String output;

    @org.junit.jupiter.api.BeforeEach
    public void CreateSturcture(){
        input = "D:/Юринфор/source.example.json";
        output = "E:/test.json";
        ReadFile file = new ReadFile(input);
        String json = file.Read();
        source = Split.deserialize(json);
    }

    @org.junit.jupiter.api.AfterEach
    public void WriteFile(){
        //System.out.println(source.toString());
        String json = Split.serialize(source);
        //System.out.println(json);
        WriteFile w = new WriteFile();
        w.Write(output, json);
    }

    @org.junit.jupiter.api.Test
    void intro() {
        output = "D:/Юринфор/Tests/intro.json";
    }

    @DisplayName("Переименование main")
    @org.junit.jupiter.api.Test
    void rename_1_1() {
        output = "D:/Юринфор/Tests/rename_1_1.json";
        Diagram diagram = source.diagrams.get("main");
        try {
            Operations.Rename(source, "main", "main_1");
        }
        catch (OperationException e) {
            assertEquals("", e.getMessage());
        }
        assertEquals(source.main, "main_1");
        assertTrue(source.diagrams.containsKey("main_1"));
        assertFalse(source.diagrams.containsKey("main"));
        assertEquals(diagram, source.diagrams.get("main_1"));
    }

    @DisplayName("Переименование не main")
    @org.junit.jupiter.api.Test
    void rename_1_2() {
        output = "D:/Юринфор/Tests/rename_1_2.json";
        try {
            Operations.Rename(source, "cond", "cond_1");
        }
        catch (OperationException e) {
            assertEquals("", e.getMessage());
        }
        assertEquals(source.main, "main");
        assertTrue(source.diagrams.containsKey("cond_1"));
        assertFalse(source.diagrams.containsKey("cond"));
    }

    @DisplayName("Переименование несуществующей диаграммы")
    @org.junit.jupiter.api.Test
    public void rename_1_3(){
        output = "D:/Юринфор/Tests/rename_1_3.json";
        try {
            Operations.Rename(source, "tred", "cond_1");
        }
        catch (OperationException e) {
            assertEquals("Диаграмма с идентификатором tred не существует!", e.getMessage());
        }
    }

    @DisplayName("Переименование в уже существующую диаграмму")
    @org.junit.jupiter.api.Test
    public void rename_1_4(){
        output = "D:/Юринфор/Tests/rename_1_4.json";
        try {
            Operations.Rename(source, "main", "cond");
        }
        catch (OperationException e) {
            assertEquals("Диаграмма с идентификатором cond уже существует!", e.getMessage());
        }
    }


    @org.junit.jupiter.api.Test
    void rename_2_1() {
        output = "D:/Юринфор/Tests/rename_2_1.json";
        try {
            Operations.Rename(source, "tred", "a", "b");
        }
        catch (OperationException e) {
            assertEquals("Диаграмма с идентификатором tred не существует!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void rename_2_2() {
        output = "D:/Юринфор/Tests/rename_2_2.json";
        try {
            Operations.Rename(source, "main", "test", "b");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main не существует блока с идентификатором test!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void rename_2_3() {
        output = "D:/Юринфор/Tests/rename_2_3.json";
        try {
            Operations.Rename(source, "main", "c42", "cnd42");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void rename_2_4() {
        output = "D:/Юринфор/Tests/rename_2_4.json";
        try {
            Operations.Rename(source, "main", "cnd42", "entr");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
    }

    /*@org.junit.jupiter.api.Test
    void rename_2_5() {
        output = "D:/Юринфор/Tests/rename_2_5.json";
        List<String> outlist = new LinkedList<>();
        Map<String, Block> blocks = source.diagrams.get("a1").blocks;
        for(Map.Entry<String, Block> entry : blocks.entrySet()){
            if(entry.getValue().getType().equals("action")){
                Action act = (Action) entry.getValue();
                if (act.next.equals("next")) outlist.add(entry.getKey());
            }
            List<StringListNode> ch = new LinkedList<>();
            if(entry.getValue().getType().equals("select")){
                Select sel = (Select) entry.getValue();
                ch = sel.children;
            }
            else if(entry.getValue().getType().equals("import")){
                Import imp = (Import) entry.getValue();
                ch = imp.children;
            }
            for(int i = 0; i < ch.size(); i++){
                if(ch.get(i).meaning.equals("next")) outlist.add(entry.getKey());
            }
        }
        try {
            Operations.Rename(source, "a1", "next", "test");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
        assertTrue(source.diagrams.get("a1").blocks.containsKey("test"));
        System.out.println(source.diagrams.get("a1").blocks.toString());
        blocks = source.diagrams.get("a1").blocks;
        for(Map.Entry<String, Block> entry : blocks.entrySet()){
            if(outlist.contains(entry.getKey())){
                switch (entry.getValue().getType()){
                    case "action":
                        assertTrue(((Action) entry.getValue()).next.equals("test"));
                        break;
                    case "import":
                        assertTrue(transform(((Import) entry.getValue()).children, true).contains("test"));
                        break;
                    case "select":
                        assertTrue(transform(((Select) entry.getValue()).children, true).contains("test"));
                        break;
                    default:
                }
            }
        }
    }*/

    public List<String> transform(Map<String, String> hash, boolean mode){

        List<String> r = new LinkedList<>();
        for(Map.Entry<String, String> l : hash.entrySet()){
            if (mode)  r.add(l.getValue());
            else r.add(l.getKey());
        }
        return r;
    }

    @Test
    void rename_2_6() {
        output = "D:/Юринфор/Tests/rename_2_6.json";
        try {
            Operations.Rename(source, "main", "skip", "entreeee");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_1() {
        output = "D:/Юринфор/Tests/include_1.json";
        try {
            Operations.Include(source, "igogo", "cnd42");
        }
        catch (OperationException e) {
            assertEquals("Диаграмма с идентификатором igogo не существует!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_2() {
        output = "D:/Юринфор/Tests/include_2.json";
        try {
            Operations.Include(source, "main", "aaaaaa");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main не существует блока с идентификатором aaaaaa!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_3() {
        output = "D:/Юринфор/Tests/include_3.json";
        try {
            Operations.Include(source, "main", "next");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main существует блок с идентификатором next, но он не является ссылкой на внешнюю диаграмму!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_4() {
        output = "D:/Юринфор/Tests/include_4.json";
        try {
            Operations.Include(source, "a1", "b");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме a1 существует блок с идентификатором b, но он не является ссылкой на внешнюю диаграмму!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_5() {
        output = "D:/Юринфор/Tests/include_5.json";
        try {
            Operations.Include(source, "main", "cnd42");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void include_6() {
        output = "D:/Юринфор/Tests/include_6.json";
        try {
            Operations.Include(source, "main", "c42");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main уже существует блок с идентификатором cnd42!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void renameOut_1() {
        output = "D:/Юринфор/Tests/renameOut_1.json";
        try {
            Operations.RenameOut(source, "main", "exit", "next1");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main не существует блока с идентификатором exit!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void renameOut_2() {
        output = "D:/Юринфор/Tests//renameOut_2.json";
        try {
            Operations.RenameOut(source, "main", "c42", "next1");
        }
        catch (OperationException e) {
            assertEquals("В диаграмме main существует блок с идентификатором c42, но он не является выходным!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void renameOut_3() {
        output = "D:/Юринфор/Tests/renameOut_3.json";
        try {
            Operations.RenameOut(source, "a2", "next", "skip");
        }
        catch (OperationException e) {
            assertEquals("Блок с идентификатором skip уже является выходным в диаграмме a2!", e.getMessage());
        }
    }

    @org.junit.jupiter.api.Test
    void renameOut_4() {
        output = "D:/Юринфор/Tests/renameOut_4.json";
        Diagram diag = source.diagrams.get("a2");
        System.out.println(diag.outs.keySet());
        System.out.println(diag.outs.toString());
        try {
            Operations.RenameOut(source, "a2", "0", "ggggg");
        }
        catch (OperationException e) {
            assertEquals("Блок с идентификатором skip уже является выходным в диаграмме a2!", e.getMessage());
        }
        System.out.println(diag.outs.get("ggggg"));
    }

    @org.junit.jupiter.api.Test
    void extract() {
    }
}