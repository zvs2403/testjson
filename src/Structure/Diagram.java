package Structure;

import java.util.List;
import java.util.Map;

public class Diagram {

    public String root;
    public Map<String, String> outs;
    public Map<String, Block> blocks;
    public String name;
    public String src;

    public Diagram(){

    }

    public Diagram(String root, Map<String, String> outs, Map<String, Block> blocks, String name, String src){
        this.root = root;
        this.outs = outs;
        this.blocks = blocks;
        this.name = name;
        this.src = src;
    }

    public Diagram(String root, Map<String, String> outs, Map<String, Block> blocks){
        this.root = root;
        this.outs = outs;
        this.blocks = blocks;
    }
}
