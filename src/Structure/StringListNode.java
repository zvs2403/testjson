package Structure;

public class StringListNode {

    public String id;
    public String meaning;

    public StringListNode(){

    }

    public StringListNode(String id, String meaning){
        this.id = id;
        this.meaning = meaning;
    }

    /*public StringListNode(String id){
        this.id = id;
        this.meaning = "";
    }*/
}
