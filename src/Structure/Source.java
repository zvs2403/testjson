package Structure;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class Source {

    @SerializedName("rootDiagram")
    public String main;

    //public List<DiagramNode> diagrams;
    public Map<String, Diagram> diagrams;

    public Source(){

    }

    public Source(String main, Map<String, Diagram> diagrams){
        this.main = main;
        this.diagrams = diagrams;
    }

    @Override
    public String toString(){
        String res = "";
        for(Map.Entry<String, Diagram> entry : diagrams.entrySet()){
            res += entry.getKey() + ":" + "\n";
            for(Map.Entry<String, Block> entry1 : entry.getValue().blocks.entrySet()){
                res += "\t" + entry1.getKey() + ":" + entry1.getValue().toString()  + "\n";
            }
            res += "\n";
        }
        return res;
    }
}
