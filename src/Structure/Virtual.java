package Structure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Virtual extends Block {

    public String name;
    public String optype;

    public Virtual(String name, String optype, JsonObject json){
        super(json);
        this.optype = optype;
        this.name = name;
    }

    @Override
    public String getType(){
        return "virtual";
    }

    @Override
    public String toString(){
        return getType();
    }
}
