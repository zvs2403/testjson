package Structure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Import extends Block {

    public String name;
    public String msg;
    public String optype;
    public String dep;
    public Map<String, String> children;

    public Import(String name, String msg, String dep, Map<String, String> children, JsonObject json) {
        super(json);
        this.name = name;
        this.msg = msg;
        this.optype = "dep";
        this.dep = dep;
        this.children = children;
    }

    @Override
    public String getType(){
        return "import";
    }

    @Override
    public String toString(){
        String res = getType() + " ";
        for(Map.Entry<String, String> ch : children.entrySet()){
            res += " " + ch.getValue();
        }
        res += ";";
        return res;
    }

    @Override
    public Import mod(String from, String to){
        for(Map.Entry<String, String> child : this.children.entrySet()){
            if(child.getValue().equals(from)) child.setValue(to);
        }
        Map<String, String> newChild = new LinkedHashMap<>(this.children);
        return new Import(this.name, this.msg, this.dep, newChild, this.json);
    }
}
