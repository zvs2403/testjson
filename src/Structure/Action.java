package Structure;

import com.google.gson.JsonObject;

public class Action extends Block {

    public String name;
    public String msg;
    public String next;

    public Action(String name, String msg, String next, JsonObject json){
        super(json);
        this.name = name;
        this.msg = msg;
        this.next = next;
    }

    @Override
    public String getType(){
        return "action";
    }

    @Override
    public String toString(){
        return getType() + " " + this.next;
    }

    @Override
    public Action mod(String from, String to) {
        if(this.next.equals(from)){
            return new Action(this.name, this.msg, to, this.json);
        }
        else {
            return this;
        }
    }

}
