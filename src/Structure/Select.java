package Structure;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Select extends Block {

    public String name;
    public String msg;
    public String optype;
    public Map<String, String> labels;
    public Map<String, String> children;
    public JsonObject jsonObject;


    public Select(String name, String msg, Map<String, String> labels,
                  Map<String, String> children, JsonObject json){
        super(json);
        this.name = name;
        this.msg = msg;
        this.optype = "Select";
        this.labels = labels;
        this.children = children;
        this.jsonObject = json;
    }

    @Override
    public String getType(){
        return "select";
    }

    @Override
    public String toString(){
        String res = getType() + " ";
        for(Map.Entry<String, String> ch: children.entrySet()){
            res += " " + ch.getValue();
        }
        res += ";";
        return res;
    }

    public Select mod(String from, String to){
        for(Map.Entry<String, String> child : this.children.entrySet()){
            if(child.getValue().equals(from)) child.setValue(to);
        }
        Map<String, String> newChild = new LinkedHashMap<>(this.children);
        return new Select(this.name, this.msg, this.labels, newChild, this.json);
    }
}
