package Operations;

import Service.*;
import Structure.*;

public class Main {

    public static void main(String[] args) {
	// write your code here

    //Скрипт для запуска jar-файла: java -jar testjson.jar [arguments]
    //Акутальный jar-файл лежит здесь: our/artifatcs/testjson_jar/testjson.jar

        //System.out.println(args.length);
        if(args.length == 0){
            System.out.println("No arguments");
            return;
        }
        else if(args.length == 1){
            System.out.println("Not enough arguments");
            return;
        }
        String inputfile = args[0];
        ReadFile file = new ReadFile(inputfile);
        String inputJsonString = file.Read();
        if(inputJsonString.equals("F")){
            return;
        }

        String command = args[1];
        String[] splitArguments = Split.split(command);

        Source source = Split.deserialize(inputJsonString);

        /*for(int i = 0 ; i < splitArguments.length; i++){
            System.out.println(splitArguments[i]);
        }*/
        switch (splitArguments[0]){
            case ("rename"):
                if(splitArguments.length == 3){
                    try{
                        Operations.Rename(source, splitArguments[1], splitArguments[2]);
                    }
                    catch(OperationException e){
                        System.out.println(e.getMessage());
                    }
                }
                else if (splitArguments.length == 4){
                    try{
                        Operations.Rename(source, splitArguments[1], splitArguments[2], splitArguments[3]);
                    }
                    catch(OperationException e){
                        System.out.println(e.getMessage());
                    }
                }
                else {
                    System.out.println(-2);
                }
                break;
            case("include"):
                if  (splitArguments.length == 4){
                    try{
                        Operations.Include(source, splitArguments[1], splitArguments[2]);
                    }
                    catch(OperationException e){
                        System.out.println(e.getMessage());
                    }
                }
                else {
                    System.out.println(-3);
                }
                break;
            case("renameOutKey"):
                if  (splitArguments.length == 4){
                    try {
                        Operations.RenameOut(source, splitArguments[1], splitArguments[2], splitArguments[3]);
                    }
                    catch(OperationException e){
                        System.out.println(e.getMessage());
                    }
                }
                else {
                    System.out.println(-4);
                }
                break;
            case("extract"):
                if  (splitArguments.length == 4){
                    Operations.Extract(inputJsonString, splitArguments[1], splitArguments[2], splitArguments[3]);
                }
                else {
                    System.out.println(-5);
                }
                break;
            default:
                System.out.println("Invalid command");
                break;
        }

        String outputJsonString = Split.serialize(source);
        System.out.println(outputJsonString);

    }
}
