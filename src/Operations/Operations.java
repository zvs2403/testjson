package Operations;

import Service.OperationException;
import Structure.*;

import java.util.*;

public class Operations {

    public Operations(){

    }

    public static void Rename (Source json, String d_id, String b_from, String b_to) throws OperationException {

        if(!json.diagrams.containsKey(d_id)){
            throw new OperationException(String.format("Диаграмма с идентификатором %s не существует!", d_id));
        }

        Diagram diagram = json.diagrams.get(d_id);
        Map<String, Block> blocks = diagram.blocks;

        if(!blocks.containsKey(b_from)){
            throw new OperationException(String.format("В диаграмме %s не существует блока с идентификатором %s!", d_id, b_from));
        }

        if(blocks.containsKey(b_to)){
            throw new OperationException(String.format("В диаграмме %s уже существует блок с идентификатором %s!", d_id, b_to));
        }

        Block block = blocks.get(b_from);
        blocks.remove(b_from);
        blocks.put(b_to, block);

        if(diagram.root.equals(b_from)){
            diagram.root = b_to;
        }

        for(Map.Entry<String, String> node : diagram.outs.entrySet()){
            if(node.getKey().equals(b_from)){
                node.setValue(b_to);
                break;
            }
        }

        for(Map.Entry<String, Block> entry : blocks.entrySet()){
            if(entry.getValue().getType().equals("action")) {
                Action act = (Action) entry.getValue();
                if (act.next.equals(b_from)){
                    act.next = b_to;
                }
            }
            else if(entry.getValue().getType().equals("select")){
                Select sel = (Select) entry.getValue();
                for(Map.Entry<String, String> child : sel.children.entrySet()){
                    if(child.getValue().equals(b_from)) child.setValue(b_to);
                }
            }
            else if(entry.getValue().getType().equals("import")){
                Import imp = (Import) entry.getValue();
                for(Map.Entry<String, String> child: imp.children.entrySet()){
                    if(child.getValue().equals(b_from)) child.setValue(b_to);
                }
            }
        }
    }

    //entri - куда писать

    public static void Rename (Source json, String d_i1, String d_i2) throws OperationException {

        //проверка наличия диаграммы с таким именем
        if(!json.diagrams.containsKey(d_i1)){
            throw new OperationException(String.format("Диаграмма с идентификатором %s не существует!", d_i1));
        }
        if(json.diagrams.containsKey(d_i2)){
            throw new OperationException(String.format("Диаграмма с идентификатором %s уже существует!", d_i2));
        }

        Diagram diagram = json.diagrams.get(d_i1);
        json.diagrams.remove(d_i1);
        json.diagrams.put(d_i2, diagram);

        if(json.main.equals(d_i1)){
            json.main = d_i2;
        }

        //поиск переименованной диаграммы в импортируемых блоках
        for(Map.Entry<String, Diagram> entryi : json.diagrams.entrySet()){
            Map<String, Block> blockList = entryi.getValue().blocks;
            for(Map.Entry<String, Block> entryj : blockList.entrySet()){
                Block block = entryj.getValue();
                if(block.getType().equals("import")){
                    Import imp = (Import) block;
                    if (imp.dep.equals(d_i1)) imp.dep = d_i2;
                }
            }
        }
    }

    public static void Include (Source json, String d_id, String b_id) throws OperationException {

        if(!json.diagrams.containsKey(d_id)){
            throw new OperationException(String.format("Диаграмма с идентификатором %s не существует!", d_id));
        }

        Diagram diagram = json.diagrams.get(d_id);
        Map<String, Block> blocks = diagram.blocks;

        System.out.println(blocks.toString());
        if(!blocks.containsKey(b_id)){
            throw new OperationException(String.format("В диаграмме %s не существует блока с идентификатором %s!", d_id, b_id));
        }

        Block target = blocks.get(b_id);

        if(!target.getType().equals("import")) {
            throw new OperationException(String.format("В диаграмме %s существует блок с идентификатором %s, но он не является ссылкой на внешнюю диаграмму!", d_id, b_id));
        }

        Import block = (Import) target;

        String targetDiagId = block.dep;

        if(!json.diagrams.containsKey(targetDiagId)){
            throw new OperationException(String.format("Импортируемая диаграмма с идентификатором %s не существует!", targetDiagId));
        }

        boolean importRoot = diagram.root.equals(b_id);
        Diagram targetDiag = json.diagrams.get(targetDiagId);
        System.out.println(targetDiagId);
        Map<String, Block> targetBlocks = new LinkedHashMap<>(targetDiag.blocks);
        Map<String, Block> duplicateBlocks = new LinkedHashMap<>();
        Map<String, Block> newBlocks = new LinkedHashMap<>();
        String root = targetDiag.root;
        blocks.remove(b_id);

        System.out.println(targetDiag.outs.size());

        System.out.println(targetBlocks.toString());

        for(Map.Entry<String, Block> entry : targetBlocks.entrySet()){
            if(blocks.containsKey(entry.getKey()) && !entry.getValue().getType().equals("virtual")) {
                String newKey = entry.getKey() + "_dup";
                Block newValue = entry.getValue();
                duplicateBlocks.put(entry.getKey(), entry.getValue());
                newBlocks.put(newKey, newValue);
            }
        }

        System.out.println(targetBlocks.toString());

        for(Map.Entry<String, Block> entry : targetBlocks.entrySet()){
            if(entry.getValue().getType().equals("action")) {
                Action act = (Action) entry.getValue();
                if(duplicateBlocks.containsKey(act.next)) act.next += "_dup";
            }
            else if(entry.getValue().getType().equals("select")){
                Select sel = (Select) entry.getValue();
                for(Map.Entry<String, String> node : sel.children.entrySet()){
                    if(duplicateBlocks.containsKey(node.getValue())) node.setValue(node.getValue() + "_dup");
                }
            }
            else if(entry.getValue().getType().equals("import")){
                Import imp = (Import) entry.getValue();
                for(Map.Entry<String, String> child : imp.children.entrySet()){
                    if(duplicateBlocks.containsKey(child.getValue())) child.setValue(child.getValue() + "_dup");
                }
            }
        }

        System.out.println(targetBlocks.toString());

        int i = 0;
        for(Map.Entry<String, String> entri : targetDiag.outs.entrySet()){
            for(Map.Entry<String, Block> entrj : targetBlocks.entrySet()){
                if(entrj.getValue().getType().equals("action")) {
                    Action act = (Action) entrj.getValue();
                    if(entri.getValue().equals(act.next)) act.next = block.children.get(entri.getKey());
                }
                else if(entrj.getValue().getType().equals("select")){
                    Select sel = (Select) entrj.getValue();
                    for(Map.Entry<String, String> child : sel.children.entrySet()){
                        if(entri.getValue().equals(child.getValue())) child.setValue(block.children.get(entri.getKey()));
                    }
                }
                else if(entrj.getValue().getType().equals("import")){
                    Import imp = (Import) entrj.getValue();
                    for(Map.Entry<String, String> child : imp.children.entrySet()){
                        if(entri.getValue().equals(child.getValue())) child.setValue(block.children.get(entri.getKey()));
                    }
                }
            }
        }

        System.out.println(targetBlocks.toString());

        for(Map.Entry<String, String> node : targetDiag.outs.entrySet()){
            targetBlocks.remove(node.getValue());
        }


        System.out.println(targetBlocks.toString());

        for(Map.Entry<String, Block> entry : duplicateBlocks.entrySet()){
            targetBlocks.remove(entry.getKey());
        }
        System.out.println(targetBlocks.toString());
        targetBlocks.putAll(newBlocks);
        System.out.println(targetBlocks.toString());
        boolean rootIsDuplicate = duplicateBlocks.containsKey(root);

        for(Map.Entry<String, Block> entry : blocks.entrySet()){
            if(entry.getValue().getType().equals("action")) {
                Action act = (Action) entry.getValue();
                if (act.next.equals(b_id)){
                    act.next = rootIsDuplicate ? root + "_dup" : root;
                }
            }
            else if(entry.getValue().getType().equals("select")){
                Select sel = (Select) entry.getValue();
                for(Map.Entry<String, String> child : sel.children.entrySet()){
                    if(child.getValue().equals(b_id)){
                        String newValue = rootIsDuplicate ? root + "_dup" : root;
                        child.setValue(newValue);
                    }
                }
            }
            else if(entry.getValue().getType().equals("import")){
                Import imp = (Import) entry.getValue();
                for(Map.Entry<String, String> child : imp.children.entrySet()){
                    if(child.getValue().equals(b_id)){
                        String newValue = rootIsDuplicate ? root + "_dup" : root;
                        child.setValue(newValue);
                    }
                }
            }
        }


        blocks.putAll(targetBlocks);

        System.out.println(blocks);

        System.out.println(importRoot);
        if (importRoot){
            diagram.root = rootIsDuplicate ? root + "_dup" : root;
        }

    }

    public static void RenameOut(Source json, String d_id, String k_from, String k_to) throws OperationException {

        if(!json.diagrams.containsKey(d_id)){
            throw new OperationException(String.format("Диаграмма с идентификатором %s не существует!", d_id));
        }

        Diagram diagram = json.diagrams.get(d_id);
        Map<String, String> outs = json.diagrams.get(d_id).outs;
        Map<String, Block> blocks = diagram.blocks;

        if(!outs.containsKey(k_from)){
            throw new OperationException(String.format("В диаграмме %s не существует блока с выходным ключом %s!", d_id, k_from));
        }

        if(outs.containsKey(k_to)){
            throw new OperationException(String.format("Диаграмма %s уже содержит блок с выходнным ключом %s!", d_id, k_to));
        }

        String oldValue = outs.get(k_from);
        System.out.println(outs.get(k_from));
        outs.remove(k_from);
        outs.put(k_to, oldValue);
        System.out.println(outs.get("ggggg"));

        for(Map.Entry<String, Diagram> diag : json.diagrams.entrySet()) {
            for (Map.Entry<String, Block> entry : diag.getValue().blocks.entrySet()) {
                if (entry.getValue().getType().equals("import")) {
                    Import imp = (Import) entry.getValue();
                    if (imp.dep.equals(d_id)) {
                        String oldValue1 = imp.children.get(k_from);
                        imp.children.remove(k_from);
                        imp.children.put(k_to, oldValue1);
                    }
                }
            }
        }

    }

    public static void Extract (String json, String d1_id, String b1_id, String d2_id) {

    }

    private static boolean SearchInList(Map<String, String> list, String name){
        boolean result = false;
        for(Map.Entry<String, String> node : list.entrySet()){
            if(node.getValue().equals(name)){
                result = true;
                break;
            }
        }
        return result;
    }

}
