package Serializer;

import Structure.*;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class BlockSerializer implements JsonSerializer<Block> {

    public JsonElement serialize(Block src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        switch (src.getType()){
            case ("select"):
                Select select = (Select) src;

                result.addProperty("name", select.name);
                if(select.msg != null) { result.addProperty("msg", select.msg); }
                result.addProperty("optype", select.optype);
                result.add("labels", getObject(select.labels));
                result.add("children", getObject(select.children));
                if(select.json != null) {
                    mergeObjects(result, select.json);
                }
                break;
            case ("import"):
                Import _import = (Import) src;
                result.addProperty("name", _import.name);
                if(_import.msg != null) { result.addProperty("name", _import.name); }
                result.addProperty("optype", _import.optype);
                result.addProperty("dep", _import.dep);
                result.add("children", getObject(_import.children));

                if(_import.json != null) {
                    mergeObjects(result, _import.json);
                }

                break;
            case ("virtual"):
                Virtual virtual = (Virtual) src;
                if(virtual.name != null) result.addProperty("name", virtual.name);
                if(virtual.optype != null) result.addProperty("optype", virtual.optype);
                if(virtual.json != null) {
                    mergeObjects(result, virtual.json);
                }
                break;
            case ("action"):
                Action action = (Action) src;

                result.addProperty("name", action.name);
                if(action.msg != null) { result.addProperty("msg", action.msg); }
                result.addProperty("next", action.next);
                if(action.json != null) {
                    mergeObjects(result, action.json);
                }
                break;
            default:
                result = src.json;
        }
        return result;
    }

    private void mergeObjects(JsonObject arg1, JsonObject arg2){

        for(Map.Entry<String, JsonElement> entry : arg2.entrySet()) {
            arg1.add(entry.getKey(), entry.getValue());
        }
    }

    private JsonElement getObject(Map<String, String> hashMap){
        JsonElement result;

        if(hashMap.isEmpty()){
            result = new JsonArray();
            return result;
        }
        boolean outKeysIsStub = true;
        int i = 0;
        for(Map.Entry<String, String> node : hashMap.entrySet()){
            if(!node.getKey().equals(String.valueOf(i))){
                outKeysIsStub = false;
                break;
            }
            i++;
        }
        if (outKeysIsStub){
            JsonArray jsonArray = new JsonArray();
            for(Map.Entry<String, String> node : hashMap.entrySet()){
                jsonArray.add(node.getValue());
            }
            result = jsonArray;
        }
        else {
            JsonObject jsonObject = new JsonObject();
            for(Map.Entry<String, String> node : hashMap.entrySet()){
                jsonObject.addProperty(node.getKey(), node.getValue());
            }
            result = jsonObject;
        }
        return result;
    }


}
