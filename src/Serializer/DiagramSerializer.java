package Serializer;

import Structure.*;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DiagramSerializer implements JsonSerializer<Diagram> {

    @Override
    public JsonElement serialize(Diagram src, Type typeOfSrc, JsonSerializationContext context){
        JsonObject result = new JsonObject();
        if(src.src != null){
            result.addProperty("src", src.src);
        }
        result.addProperty("root", src.root);

        if(src.outs.isEmpty()){
            result.add("outs", context.serialize(src.outs));
        }
        boolean outKeysIsStub = true;
        int i = 0;
        for(Map.Entry<String, String> out : src.outs.entrySet()){
            if(!out.getKey().equals(String.valueOf(i))){
                outKeysIsStub = false;
                break;
            }
            i++;
        }

        if (outKeysIsStub){
            JsonArray jsonArray = new JsonArray();
            for(Map.Entry<String, String> out : src.outs.entrySet()){
                jsonArray.add(out.getValue());
            }
            result.add("outs", jsonArray);

        }
        else {
            JsonObject jsonObject = new JsonObject();
            for(Map.Entry<String, String> out : src.outs.entrySet()){
                jsonObject.addProperty(out.getKey(), out.getValue());
            }
            result.add("outs", jsonObject);
        }
        if(src.name != null) {
            result.addProperty("name", src.name);
        }
        JsonObject jsonObject = new JsonObject();
        //jsonObject.add(src.root, context.serialize(src.blocks.get(src.root)));
        List<String> outslist = new LinkedList<>();
        List<String> virtuallist = new LinkedList<>();
        outslist.add(src.root);

        /*for(int i = 0; i < outslist.size(); i++){
            if(src.blocks.get(outslist.get(i)).getType().equals("action")){
                Action act = (Action) src.blocks.get(outslist.get(i));
                if(!Search(outslist, act.next)) {
                    outslist.add(act.next);
                }
            }
            else if(src.blocks.get(outslist.get(i)).getType().equals("import")){
                Import imp = (Import) src.blocks.get(outslist.get(i));
                List<StringListNode> list = imp.children;
                for(int j = 0; j < list.size(); j++){
                    if(!Search(outslist, list.get(j).meaning)){
                        outslist.add(list.get(j).meaning);
                    }
                }
            }
            else if(src.blocks.get(outslist.get(i)).getType().equals("select")){
                Select sel = (Select) src.blocks.get(outslist.get(i));
                List<StringListNode> list = sel.children;
                for(int j = 0; j < list.size(); j++){
                    if(!Search(outslist, list.get(j).meaning)){
                        outslist.add(list.get(j).meaning);
                    }
                }
            }

        }

        for(int i = 0; i < outslist.size(); i++){
            jsonObject.add(outslist.get(i), context.serialize(src.blocks.get(outslist.get(i))));
        }*/
        for(Map.Entry<String, Block> entry : src.blocks.entrySet()){
            jsonObject.add(entry.getKey(), context.serialize(entry.getValue()));
        }
        result.add("blocks", jsonObject);

        return result;
    }

    private boolean Search(List<String> list, String name){
        boolean result = false;
        for(String node : list){
            if(node.equals(name)){
                result = true;
            }
        }
        return result;
    }


}
