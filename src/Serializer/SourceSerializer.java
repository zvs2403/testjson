package Serializer;

import Structure.Diagram;
import Structure.Source;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class SourceSerializer implements JsonSerializer<Source> {

    @Override
    public JsonElement serialize(Source src, Type typeOfSrc, JsonSerializationContext context){
        JsonObject result = new JsonObject();
        result.addProperty("rootDiagram", src.main);
        //result.addProperty("test", "rrrrr");
        //result.add("diagrams", context.serialize(src.diagrams));
        JsonObject jsonObject = new JsonObject();
        Map<String, Diagram> copy = new HashMap<>(src.diagrams);
        jsonObject.add(src.main, context.serialize(copy.get(src.main)));
        copy.remove(src.main);
        for (Map.Entry<String, Diagram> entry : copy.entrySet()){
            jsonObject.add(entry.getKey(), context.serialize(entry.getValue()));
        }
        result.add("diagrams", jsonObject);
        return result;
    }
}
