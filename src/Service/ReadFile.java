package Service;

import java.io.*;

public class ReadFile {

    private String inputfile;

    public ReadFile(String input){

        this.inputfile = input;
    }

    public String Read(){

        StringBuilder output = new StringBuilder();
        try{
            FileReader file = new FileReader(inputfile);
            BufferedReader reader = new BufferedReader(file);
            String nextline;

            while ((nextline = reader.readLine()) != null){
                output.append(nextline).append("\n");
            }

        }
        catch(FileNotFoundException e){
            System.out.println(e.getMessage());
            return ("F");
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            return ("F");
        }
        return output.toString();
    }
}
