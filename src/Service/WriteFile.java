package Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {

    public void Write(String file, String json){

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file)))
        {
            bw.write(json);
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
