package Service;

import Deserializer.DiagramDeserializer;
import Deserializer.BlockDeserializer;
import Deserializer.SourceDeserializer;
import Serializer.*;
import Structure.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.regex.Pattern;

public class Split {


    public Split(){

    }

    public static String[] split(String input){
        String regex = "\\s+";
        Pattern pattern = Pattern.compile(regex);
        String[] splitinput = pattern.split(input);
        return splitinput;
    }

    public static String serialize(Source source){

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Source.class, new SourceSerializer())
                //.registerTypeAdapter(DiagramNode.class, new DiagramListSerializer())
                .registerTypeAdapter(Diagram.class, new DiagramSerializer())
                //.registerTypeAdapter(StringListNode.class, new OutSerializer())
                //.registerTypeAdapter(Block.class, new BlockSerializer())
                .registerTypeAdapter(Block.class, new BlockSerializer())
                .registerTypeAdapter(Import.class, new BlockSerializer())
                .registerTypeAdapter(Select.class, new BlockSerializer())
                .registerTypeAdapter(Action.class, new BlockSerializer())
                .registerTypeAdapter(Virtual.class, new BlockSerializer())
                .create();

        String jsonString = gson.toJson(source);
        return jsonString;
    }

    public static Source deserialize(String json){

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Source.class, new SourceDeserializer())
                .registerTypeAdapter(Diagram.class, new DiagramDeserializer())
                .registerTypeAdapter(Block.class, new BlockDeserializer())
                .create();

        Source source = gson.fromJson(json, Source.class);
        return source;
    }

}
